---
name: Code of Conduct
description: Staff Code of Conduct
aliases: [coc, conduct]
---

# Code of Conduct

- All contributors are welcome regardless of their background.
- All contributors should act respectfully of each other and make an effort to cooperate especially with those they work most closely with.
- All humans can be mistaken and learn from their errors. Including staff.
- Mana Team members should be mindful that, as the executive body of Manasource, they are representatives of the organisation and should behave accordingly.
- We believe in a positive and cooperative attitude, open discussion, mutual respect and tolerance even in the face of the worst disagreements.
